from flask import Flask,redirect,url_for,request
#flask_login import login_required
app = Flask(__name__)

@app.route('/admin')
def admin():
    return 'This is admin'
@app.route('/Employee/<username>')
def employee(username):
    return "Employee's name is %s" % username
@app.route('/invalid')
def invalid():
    return"Invalid query"
@app.route('/')
def trial():
    _control={}
    _control['username']=request.args.get('username')
    _control['type']=request.args.get('type')
    if _control['type']=='admin':
        return redirect(url_for('admin'))
    elif _control['type']=='employee':
        return redirect(url_for('employee',username=_control['username']))
    else:
        return redirect(url_for('invalid'))
if __name__ == "__main__":
    app.run(debug=True)
